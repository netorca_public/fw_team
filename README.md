# Firewall rules

This repo is a demonstration of how a firewall rule could be defined in netorca

The rule schema defined here supports, network addresses, hostnames or an optional pointer to a VM defined within NetOrca. 
Regex validation is provided for each. 

This can be expanded to support many different methods of firewall rule requesting, whilst all being maintained within the same user configuration. 